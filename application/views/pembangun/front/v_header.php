<html>

<head>
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="N-Air Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() {setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<meta charset utf="8">
		<!--fonts-->
		<link href='//fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'>
		
		<!--fonts-->
        <!--form-css-->
        <link href="<?php echo base_url()?>assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
		<!--bootstrap-->
			 <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<!--coustom css-->
			<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <!--shop-kart-js-->
        <script src="<?php echo base_url()?>assets/js/simpleCart.min.js"></script>
		<!--default-js-->
			<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>
		<!--bootstrap-js-->
			<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
		<!--script-->
         <!-- FlexSlider -->
            <script src="<?php echo base_url()?>assets/js/imagezoom.js"></script>
              <script defer src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
            <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css" type="text/css" media="screen" />


</head>





<body>
	 <!-- nav -->
	<div class="container">
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">Burung Shop</a>
		    </div>
		    <ul class="nav navbar-nav">
		      <li class="active"><a href="<?php echo base_url()?>home">Home</a></li>
		      <li><a href="<?php echo base_url()?>burung">Burung</a></li> 
		      <li><a href="<?php echo base_url()?>komfirmasipembayaran">Konfirmasi Pembayaran</a></li>
		      <li><a href="<?php echo base_url()?>kirimpesan">Hubungin Kami</a></li>
		      <li><a href="#">Profil</a></li> 
		    </ul>
		    <ul class="nav navbar-nav navbar-right">
		      <?php

					if($this->session->userdata('status') != "login"){
						
						echo "<li><a href='";
						echo base_url();
						echo "registrasi'><span class='glyphicon glyphicon-user'></span>Sign Up</a></li>";
						
						
						
						echo "<li><a href='#' data-toggle='modal' data-target='#login' ><span class='glyphicon glyphicon-log-in'></span> Login</a></li>";
				

					
					}else{


		
						echo "<li><a href='#'><span class='glyphicon glyphicon-user'></span>";
						echo $this->session->userdata("nama");
						echo "</a></li>";
						echo "<li><a href='";
						echo base_url();
						echo "login/logout'>";
						echo "<span class='glyphicon glyphicon-user'></span>Logout</a></li>Halo";
						
						
						

					}


				?>
				
		    </ul>
		  </div>
		</nav>
	
	<!-- end nav -->
	
	
	<!-- Modal -->
	<div id="login" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><h1>Login</h1></h4>
		  </div>
		  <div class="modal-body">
						
						<?php

                        $methodform='method="post"';
                        $enctypeform='enctype="multipart/form-data"';
                        $classform='class="form-horizontal"';

                        $atribut = array(

                          'class' =>'form-horizontal',
                          'method' =>'post',
                          
                         );
                        ?>

                        <?php echo form_open('login/aksi_login',$atribut); ?> 
                        <div class="form-group">
                            <label for="username" class="col-xs-2">Username</label>
                            <div class="col-xs-8">
                                <input type="text" name="username" class="form-control" placeholder="Username" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email"  class="col-xs-2">Password</label>
                            <div class="col-xs-8">
                                <input type="password" name="password" class="form-control" placeholder="Password" />
                            </div>
                        </div>

                        
                        <div  class="col-xs-10 col-xs-offset-4">
                            <button type="submit" class="btn btn-danger">Login</button>
                           
                        </div>
                    	</form>
						

		  </div>
		  <div class="modal-footer">
			
		  </div>
		</div>

	  </div>
	</div>
	<!-- end modal--!>

	