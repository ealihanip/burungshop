
<?php 
 
class M_adminburung extends CI_Model{	
	function simpan($table,$data){	
		
		return $this->db->insert($table,$data);
	}	

	function ambil($table){
		return $this->db->get($table);
	}

	function jumlah_data($table){
		return $this->db->get($table)->num_rows();
	}

	function hapus($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function data($table,$number,$offset){
		return $query = $this->db->get($table,$number,$offset)->result();		
	}


	function edit($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	
}