
<?php 
 
class M_burung extends CI_Model{	
	
	function ambil($where,$table){		
	
	return $this->db->get_where($table,$where);
	
	}

	function jumlah_data($table){
		return $this->db->get($table)->num_rows();
	}

	
	function data($table,$number,$offset){
		return $query = $this->db->get($table,$number,$offset)->result();		
	}
	
	function ambildatapembelian($where,$table){
		$this->db->select('pembelian.idpembelian, pembelian.iduser, user.nama, user.alamat,user.nohp , pembelian.idburung, burung.namaburung, burung.hargaburung');
		$this->db->from('pembelian');
		$this->db->join('user', 'pembelian.iduser = user.iduser');
		$this->db->join('burung', 'pembelian.idburung = burung.idburung');
		return $this->db->get_where('',$where);		
	}
	
	function simpan($table,$data){	
		
		return $this->db->insert($table,$data);
	}	

		
}