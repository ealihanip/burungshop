<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kirimpesan extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_kirimpesan');
		$this->load->library('form_validation');
	}

	
	public function index()
	{
		
		$this->load->view('pembangun/front/v_header');
		$this->load->view('kirimpesan/v_kirimpesan');
		$this->load->view('pembangun/front/v_footer');

	}




	public function simpan(){


		//proses form validation
		
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('nohp','Nohp','required');
		$this->form_validation->set_rules('pesan','Pesan','required');
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		

		
		if($this->form_validation->run() != false){

	        
				
				$nama = $this->input->post('nama');
				$nohp= $this->input->post('nohp');
				$pesan = $this->input->post('pesan');

				
				
					
				$data = array(


					'namacostumer' => $nama,
					'nohpcostumer' => $nohp,
					'pesan' => $pesan,
					
					
					);

				$table="pesan";
				$this->m_kirimpesan->simpan($table,$data);


				redirect(base_url("home"));
			}
			
			
							
		else {

			
			$this->load->view('pembangun/front/v_header');
			$this->load->view('registrasi/v_registrasi');
			$this->load->view('pembangun/front/v_footer');



		}
		
	
	}


	
}

