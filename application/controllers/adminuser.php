<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminuser extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_adminuser');
		
		if($this->session->userdata('level') != "admin"){
			
			redirect(base_url("burung"));
		
		}
	}

	
	public function index()
	{
		


		$table="user";

		$jumlah_data = $this->m_adminuser->jumlah_data($table);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'adminuser/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		

		$data[$table] = $this->m_adminuser->data($table,$config['per_page'],$from);
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminuser/v_datauser',$data);
		$this->load->view('pembangun/admin/v_footer');
	}




	public function hapus($iduser){

		$table ="user";
		
		$where = array(

			'iduser' => $iduser

		);
		$this->m_adminuser->hapus($where,$table);
		redirect(base_url("adminuser"));


	}



	
}

