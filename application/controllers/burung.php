<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Burung extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_burung');
		$this->load->library('form_validation');
		
	}

	public function index()
	{
		
		$table="burung";

		$jumlah_data = $this->m_burung->jumlah_data($table);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'burung/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 6;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		
		$data[$table] = $this->m_burung->data($table,$config['per_page'],$from);
		
		
		$this->load->view('pembangun/front/v_header');
		$this->load->view('produk/v_tampilproduk',$data);
		$this->load->view('pembangun/front/v_footer');
		
		
		
	}
	
	public function detail($id){
		
		$where = array('idburung' => $id);
		$table = "burung";
		$data[$table] = $this->m_burung->ambil($where,$table)->result();
		
		
		$this->load->view('pembangun/front/v_header');
		$this->load->view('produk/v_tampildetailproduk',$data);
		$this->load->view('pembangun/front/v_footer');
		
	}
	
	
	public function beli($id){
		
			if($this->session->userdata('level') != "costumer"){
				echo "<script>";
				echo "alert('Silahkan Login Terlebih Dahulu')";
				echo "</script>";
				
					
			
		
			}else{
				
				$idburung = $id;
				$iduser = $this->session->userdata("iduser");
				
				$data = array(

						'iduser' => $iduser,
						'idburung' => $idburung,
						
				);
				
				
					$table="pembelian";
					$this->m_burung->simpan($table,$data);

					echo "<script>";
					echo "alert('Terima Kasih')";
					echo "</script>";
					
					$where = array('iduser' => $iduser,'idburung' => $idburung);
					$table = "pembelian";
					$data= $this->m_burung->ambil($where,$table)->row();
					
					$idpembelian=$data->idpembelian;
					
					$where = array('idpembelian' => $idpembelian,);
					$table = "pembelian";
					$data= $this->m_burung->ambildatapembelian($where,$table)->row();
					

					$this->load->view('pembangun/front/v_header');
					$this->load->view('produk/v_kwitansi',$data);
					$this->load->view('pembangun/front/v_footer');
					
					
				
					
				
			}
		
			
				
			
	}

}

