<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminpembelian extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_adminpembelian');
		
		if($this->session->userdata('level') != "admin"){
			
			redirect(base_url("home"));
		
		}
	}

	
	public function index()
	{
		


		$table="pembelian";

		$jumlah_data = $this->m_adminpembelian->jumlah_data($table);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'adminpembelian/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		

		$data[$table] = $this->m_adminpembelian->data($table,$config['per_page'],$from);
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminpembelian/v_datapembelian',$data);
		$this->load->view('pembangun/admin/v_footer');
	}




	public function hapus($idpembelian){

		$table ="pembelian";
		
		$where = array(

			'idpembelian' => $idpembelian

		);
		$this->m_adminpembelian->hapus($where,$table);
		redirect(base_url("adminkirimpesan"));


	}



	
}

