<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_registrasi');
		$this->load->library('form_validation');
	}

	
	public function index()
	{
		
		$this->load->view('pembangun/front/v_header');
		$this->load->view('registrasi/v_registrasi');
		$this->load->view('pembangun/front/v_footer');

	}




	public function simpan(){


		//proses form validation
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[kpassword]');
		$this->form_validation->set_rules('kpassword','Komfirmasi password','required');
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('nohp','Nohp','required');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		$this->form_validation->set_message('matches', '%s harus sama');
		

		
		if($this->form_validation->run() != false){

	        
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$kpassword = $this->input->post('kpassword');
				$nama = $this->input->post('nama');
				$nohp= $this->input->post('nohp');
				$alamat = $this->input->post('alamat');

				$pass=md5($password);
				$level="costumer";
					
				$data = array(

					'username' => $username,
					'password' => $pass,
					'level' => $level,
					'nama' => $nama,
					'nohp' => $nohp,
					'alamat' => $alamat,
					
					
					);

				$table="user";
				$this->m_registrasi->simpan($table,$data);


				redirect(base_url("burung"));
			}
			
			
							
		else {

			
			$this->load->view('pembangun/front/v_header');
			$this->load->view('registrasi/v_registrasi');
			$this->load->view('pembangun/front/v_footer');



		}
		
	
	}


	
}

