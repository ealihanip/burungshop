<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminburung extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_adminburung');
		$this->load->library('form_validation');
		
		if($this->session->userdata('level') != "admin"){
			
			redirect(base_url("home"));
		
		}

	}
	
	public function index()
	{
			echo "<script>";
			echo "alert('Selamat Datang Admin')";
			echo "</script>";
		
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminburung/v_homeadmin');
		$this->load->view('pembangun/admin/v_footer');


	}
	
	
	public function ldataburung(){
		
		$table="burung";

		$jumlah_data = $this->m_adminburung->jumlah_data($table);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'adminburung/ldataburung/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		

		$data[$table] = $this->m_adminburung->data($table,$config['per_page'],$from);
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminburung/v_databurung',$data);
		$this->load->view('pembangun/admin/v_footer');
	}
	
	
	public function inputdataburung()
	{
		
		
			
		
		
		
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminburung/v_inputdataburung');
		$this->load->view('pembangun/admin/v_footer');


	}

	public function simpan(){

	
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('jenis','Jenis','required');
		$this->form_validation->set_rules('harga','Harga','required|numeric');
		
		$this->form_validation->set_message('numeric', '%s harus angka');
		$this->form_validation->set_message('required', '%s gak boleh kosong');
		
		
		if($this->form_validation->run() != false){
			//proses ambil foto

	        $config['upload_path']          = './assets/gambar/';
			$config['allowed_types']        = 'gif|jpg|png';
			$this->load->library('upload', $config);
			
			
			

			if($this->upload->do_upload('foto')){

				$foto=$this->upload->file_name;
				$nama = $this->input->post('nama');
				$jenis = $this->input->post('jenis');
				$harga = $this->input->post('harga');
				$deskripsi = $this->input->post('deskripsi');
				
				
				
				$data = array(

					'namaburung' => $nama,
					'hargaburung' => $harga,
					'jenisburung' => $jenis,
					'deskripsiburung' => $deskripsi,
					'foto' => $foto,
					);


				$table="burung";
				$this->m_adminburung->simpan($table,$data);
				redirect(base_url("adminburung/ldataburung"));
			}
			
			else{


				
				$nama = $this->input->post('nama');
				$jenis = $this->input->post('jenis');
				$harga = $this->input->post('harga');
				$deskripsi = $this->input->post('deskripsi');
				
				$foto="nopic.jpg";
					
				$data = array(

					'namaburung' => $nama,
					'hargaburung' => $harga,
					'jenisburung' => $jenis,
					'deskripsiburung' => $deskripsi,
					'foto' => $foto,

					);


				$table="burung";
				$this->m_adminburung->simpan($table,$data);
				redirect(base_url("adminburung/ldataburung"));

							
				
			}



		}
		else {

			

			
			$this->load->view('pembangun/admin/v_header');
			$this->load->view('adminburung/v_inputdataburung');
			$this->load->view('pembangun/admin/v_footer');


		}
		

	
	}



	public function edit(){

		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('jenis','Jenis','required');
		$this->form_validation->set_rules('harga','Harga','required|numeric');
		
		$this->form_validation->set_message('numeric', '%s harus angka');
		$this->form_validation->set_message('required', '%s gak boleh kosong');
		

		
		if($this->form_validation->run() != false){


			$config['upload_path']          = './assets/gambar/';
			$config['allowed_types']        = 'gif|jpg|png';
			$this->load->library('upload', $config);
			

			if($this->upload->do_upload('foto')){

				$id = $this->input->post('id');
				$nama = $this->input->post('nama');
				$harga = $this->input->post('harga');
				$deskripsi = $this->input->post('deskripsi');
				$foto=$this->upload->file_name;
				
					
				$data = array(

					'namaburung' => $nama,
					'jenisburung' => $harga,
					
					'hargaburung' => $harga,
					'deskripsiburung' => $deskripsi,
					'foto' => $foto

					);

				$where = array(

					'idburung' => $id

				);

				$table="burung";
				$this->m_adminburung->edit($where,$data,$table);
				redirect(base_url("adminburung/ldataburung"));
			}
			else{


				$id = $this->input->post('id');
				$nama = $this->input->post('nama');
				$harga = $this->input->post('harga');
				$deskripsi = $this->input->post('deskripsi');
				
				
					
				$data = array(

					'namaburung' => $nama,
					'jenisburung' => $nama,
					
					'hargaburung' => $harga,
					'deskripsiburung' => $deskripsi,

					);

				$where = array(

					'idburung' => $id

				);

				$table="burung";
				$this->m_obat->edit($where,$data,$table);
				redirect(base_url("adminburung/ldataburung"));

				
			}

		}

		else{

			$table="burung";

			$jumlah_data = $this->m_adminburung->jumlah_data($table);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'adminburung/ldata';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 5;
			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);		
			

			$data[$table] = $this->m_adminburung->data($table,$config['per_page'],$from);
			$this->load->view('pembangun/admin/v_header');
			$this->load->view('adminburung/v_databurung',$data);
			$this->load->view('pembangun/admin/v_footer');


		}

				
	}


	public function hapus($id){
		
		
		$table ="burung";
		
		$where = array(

			'idburung' => $id

		);
		$this->m_adminburung->hapus($where,$table);
		redirect(base_url("adminburung/ldataburung"));

		
	}



}

