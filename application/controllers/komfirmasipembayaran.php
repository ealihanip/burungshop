<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Komfirmasipembayaran extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_komfirmasipembayaran');
		$this->load->library('form_validation');
	}

	
	public function index()
	{
		
		$this->load->view('pembangun/front/v_header');
		$this->load->view('komfirmasipembayaran/v_komfirmasipembayaran');
		$this->load->view('pembangun/front/v_footer');

	}




	public function simpan(){


		//proses form validation
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('idpembelian','Idpembelian','required');
		$this->form_validation->set_rules('bayar','Bayar','required');
		$this->form_validation->set_rules('bank','Bank','required');
		$this->form_validation->set_rules('atasnama','Atasnama','required');
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		
		

		
		if($this->form_validation->run() != false){

	        
				$nama = $this->input->post('nama');
				$idpembelian = $this->input->post('idpembelian');
				$bayar = $this->input->post('bayar');
				$bank = $this->input->post('bank');
				$atasnama = $this->input->post('atasnama');
				
				
				
				$data = array(

					
					'nama' => $nama,
					'idpembelian' => $idpembelian,
					'bayar' => $bayar,
					'bank' => $bank,
					'atasnama' => $atasnama,
					
					
					
					
					);

				$table="komfirmasipembayaran";
				$this->m_komfirmasipembayaran->simpan($table,$data);

				$this->load->view('pembangun/front/v_header');
				$this->load->view('komfirmasipembayaran/v_komfirmasipembayaranselesai');
				$this->load->view('pembangun/front/v_footer');
			}
			
			
							
		else {

			
			$this->load->view('pembangun/front/v_header');
			$this->load->view('registrasi/v_registrasi');
			$this->load->view('pembangun/front/v_footer');



		}
		
	
	}


	
}

