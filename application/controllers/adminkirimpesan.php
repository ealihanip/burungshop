<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminkirimpesan extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_adminkirimpesan');
		
		if($this->session->userdata('level') != "admin"){
			
			redirect(base_url("home"));
		
		}
	}

	
	public function index()
	{
		


		$table="pesan";

		$jumlah_data = $this->m_adminkirimpesan->jumlah_data($table);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'adminkirimpesan/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		

		$data[$table] = $this->m_adminkirimpesan->data($table,$config['per_page'],$from);
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminkirimpesan/v_kirimpesan',$data);
		$this->load->view('pembangun/admin/v_footer');
	}




	public function hapus($idpesan){

		$table ="pesan";
		
		$where = array(

			'idpesan' => $idpesan

		);
		$this->m_adminkirimpesan->hapus($where,$table);
		redirect(base_url("adminkirimpesan"));


	}



	
}

