<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();			
		
		$this->load->library('form_validation');
	}
	 
	public function index()
	{
		$this->load->view('pembangun/front/v_header');
		$this->load->view('home/v_konten');
		$this->load->view('pembangun/front/v_footer');
	}
	
	
	
	
}

