<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminkomfirmasipembayaran extends CI_Controller {

	function __construct(){
		parent::__construct();			
		$this->load->model('m_adminkomfirmasipembayaran');
		
		if($this->session->userdata('level') != "admin"){
			
			redirect(base_url("burung"));
		
		}
	}

	
	public function index()
	{
		


		$table="komfirmasipembayaran";

		$jumlah_data = $this->m_adminkomfirmasipembayaran->jumlah_data($table);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'adminuser/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		

		$data[$table] = $this->m_adminkomfirmasipembayaran->data($table,$config['per_page'],$from);
		$this->load->view('pembangun/admin/v_header');
		$this->load->view('adminkomfirmasipembayaran/v_datakomfirmasipembayaran',$data);
		$this->load->view('pembangun/admin/v_footer');
	}




	public function hapus($id){

		
		
		
		
		$where = array('idkomfirmasi' => $id,);
		$table = "komfirmasipembayaran";
		$data= $this->m_adminkomfirmasipembayaran->ambil($where,$table)->row();
					
		$idpembelian=$data->idpembelian;
		
		
		
		
		$table ="sudahdibayar";
		
		$data = array(

					
					
					'idpembelian' => $idpembelian,
					
												
					);

		$table="sudahdibayar";
		$this->m_adminkomfirmasipembayaran->simpan($table,$data);
		
		
		$table ="komfirmasipembayaran";
		
		$where = array(

			'idkomfirmasi' => $id

		);
		$this->m_adminkomfirmasipembayaran->hapus($where,$table);
		
		
		
		
		redirect(base_url("adminkomfirmasipembayaran"));


	}



	
}

