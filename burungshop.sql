/*
SQLyog Professional v12.09 (32 bit)
MySQL - 5.6.21 : Database - burungshop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`burungshop` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `burungshop`;

/*Table structure for table `burung` */

DROP TABLE IF EXISTS `burung`;

CREATE TABLE `burung` (
  `idburung` int(11) NOT NULL AUTO_INCREMENT,
  `namaburung` varchar(255) NOT NULL,
  `jenisburung` varchar(255) NOT NULL,
  `hargaburung` int(11) NOT NULL,
  `deskripsiburung` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  PRIMARY KEY (`idburung`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `burung` */

insert  into `burung`(`idburung`,`namaburung`,`jenisburung`,`hargaburung`,`deskripsiburung`,`foto`) values (14,'klsdnfk','kenari',20000,'klsdnfklsndf','31.jpg'),(15,'knsdj','kenari',100000,'jasndjansdjk','111.jpg'),(16,'ksndfkn','kenaro',20000,',amnsd,mn','1111.jpg'),(17,'ksndfkn','kenaro',20000,',amnsd,mn','1112.jpg');

/*Table structure for table `komfirmasipembayaran` */

DROP TABLE IF EXISTS `komfirmasipembayaran`;

CREATE TABLE `komfirmasipembayaran` (
  `idkomfirmasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `idpembelian` int(11) NOT NULL,
  `bayar` int(11) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `atasnama` varchar(255) NOT NULL,
  PRIMARY KEY (`idkomfirmasi`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `komfirmasipembayaran` */

insert  into `komfirmasipembayaran`(`idkomfirmasi`,`nama`,`idpembelian`,`bayar`,`bank`,`atasnama`) values (3,'ali hanifa',44,100000,'bca','Ali Hanifa'),(4,'ali hanifa',44,100000,'bca','Ali Hanifa');

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `idpembelian` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idburung` int(11) NOT NULL,
  PRIMARY KEY (`idpembelian`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `pembelian` */

insert  into `pembelian`(`idpembelian`,`iduser`,`idburung`) values (3,2,5),(7,2,5),(8,2,5),(9,2,5),(10,2,5),(11,2,5),(12,2,4),(13,2,4),(14,2,4),(15,2,4),(16,2,4),(17,2,4),(18,2,4),(19,2,4),(20,2,4),(21,2,4),(22,2,4),(23,2,4),(24,2,4),(25,2,4),(26,2,4),(27,2,4),(28,2,4),(29,2,4),(30,2,4),(31,2,4),(32,2,4),(33,2,4),(34,2,4),(35,2,4),(36,2,4),(37,2,4),(38,2,4),(39,2,4),(40,2,4),(41,2,0),(42,2,0),(43,2,0),(44,3,8),(45,3,8);

/*Table structure for table `pesan` */

DROP TABLE IF EXISTS `pesan`;

CREATE TABLE `pesan` (
  `idpesan` int(11) NOT NULL AUTO_INCREMENT,
  `namacostumer` varchar(255) NOT NULL,
  `nohpcostumer` varchar(255) NOT NULL,
  `pesan` text NOT NULL,
  PRIMARY KEY (`idpesan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pesan` */

insert  into `pesan`(`idpesan`,`namacostumer`,`nohpcostumer`,`pesan`) values (1,'kanep','08999999384','kok gitu'),(2,'Kanep','0857777777','woi ini kenapa kiriman gak nyampe nyampe');

/*Table structure for table `sudahdibayar` */

DROP TABLE IF EXISTS `sudahdibayar`;

CREATE TABLE `sudahdibayar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpembelian` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sudahdibayar` */

insert  into `sudahdibayar`(`id`,`idpembelian`) values (1,44);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nohp` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`iduser`,`username`,`password`,`level`,`nama`,`nohp`,`alamat`) values (1,'admin','c93ccd78b2076528346216b3b2f701e6','admin','','','0'),(3,'kanep','0fdaa46aae742a33f839ecb83f82058d','costumer','Ali Hanifa','0865746574657','bandung');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
